---
name: Co-branding
---

Co-branded lockups are created by the Brand Design team for approved GitLab partnerships, events, and campaigns. We default to the partner company’s co-branding lockup standards, but adhere to the following when such standards do not exist:

- Use the core GitLab logo.
- Ensure both logos are visually equal in size.
- Insert a gray line (Gray 03) between the logos equal to the height of the GitLab logo.
- Center both logos vertically; the company’s logo should not exceed the height of the tanuki.

Ensure the clear space (x-height equal to the height of the a from our wordmark) around all sides of both logos.

<figure class="figure" role="figure" aria-label="Co-branding lockup with clear space equal to the width of the lowercase 'a' from the wordmar">
  <img class="figure-img p-a-5" src="/img/brand/co-branding.svg" alt="Co-branding guidelines" role="img" />
  <figcaption class="figure-caption">Co-branding lockup with clear space equal to the width of the lowercase "a" from the wordmark</figcaption>
</figure>
